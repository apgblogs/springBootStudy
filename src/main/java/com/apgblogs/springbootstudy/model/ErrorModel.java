package com.apgblogs.springbootstudy.model;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-05-14 23:06
 */
public class ErrorModel {

    private String msg;

    private int code;

    public ErrorModel(String msg, int code) {
        this.msg = msg;
        this.code = code;
    }

    public ErrorModel(String msg, ErrorStatus errorStatus) {
        this.msg = msg;
        this.code = errorStatus.getCode();
    }

    public ErrorModel(ErrorStatus errorStatus) {
        this.msg=errorStatus.getMsg();
        this.code=errorStatus.getCode();
    }

    public String getError() {
        return msg;
    }

    public void setError(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
