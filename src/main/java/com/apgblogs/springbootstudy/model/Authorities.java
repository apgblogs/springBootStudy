package com.apgblogs.springbootstudy.model;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-05-15 16:57
 */
public enum Authorities {

    ROLE_ANONYMOUS,
    ROLE_USER,
    ROLE_ADMIN
}
