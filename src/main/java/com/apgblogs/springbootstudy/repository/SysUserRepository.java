package com.apgblogs.springbootstudy.repository;

import com.apgblogs.springbootstudy.entity.SysUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-05-15 13:02
 */
public interface SysUserRepository extends JpaRepository<SysUserEntity,String> {

    @Query("select u from SysUserEntity u where LOWER(u.username) = lower(:username)")
    SysUserEntity findByUsernameCaseInsensitive(@Param("username") String username);
}
