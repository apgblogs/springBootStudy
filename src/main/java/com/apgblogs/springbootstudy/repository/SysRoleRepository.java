package com.apgblogs.springbootstudy.repository;

import com.apgblogs.springbootstudy.entity.SysRoleEntity;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-05-15 13:07
 */
public interface SysRoleRepository extends JpaRepository<SysRoleEntity,String> {
}
