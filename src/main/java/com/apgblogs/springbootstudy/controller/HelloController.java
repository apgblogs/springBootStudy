package com.apgblogs.springbootstudy.controller;

import com.apgblogs.springbootstudy.task.TaskOne;
import com.apgblogs.springbootstudy.task.TaskTwo;
import com.apgblogs.springbootstudy.util.QuartzJobUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-06-01 12:37
 */
@RestController
public class HelloController {

    @Autowired
    private QuartzJobUtil quartzJobUtil;

    @GetMapping("/hello")
    public String index(){
        return "hello world";
    }

    @GetMapping("/quartz")
    public String addJob(){
//        quartzJobUtil.deleteJob("我们的job","task");
        quartzJobUtil.addJob(TaskTwo.class,"我们的Job","task","* * * * * ?");
        return "ok";
    }

}
