package com.apgblogs.springbootstudy.exception;

import com.apgblogs.springbootstudy.model.ErrorStatus;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-07-13 11:40
 */
public class WarningException extends GlobalException {

    public WarningException(ErrorStatus errorStatus) {
        super(errorStatus);
    }

    public WarningException(String message, ErrorStatus errorStatus) {
        super(message, errorStatus);
    }
}
