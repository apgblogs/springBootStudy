package com.apgblogs.springbootstudy.exception;


import com.apgblogs.springbootstudy.model.ErrorStatus;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-05-14 23:23
 */
public class GlobalException extends Exception {

    private int code;

    public GlobalException(ErrorStatus errorStatus){
        super(errorStatus.getMsg());
        this.code=errorStatus.getCode();
    }

    public GlobalException(String message)
    {
        super(message);
    }

    public GlobalException(String message, ErrorStatus errorStatus)
    {
        super(message);
        this.code = errorStatus.getCode();
    }

    public void setCode(int code)
    {
        this.code = code;
    }

    public int getCode()
    {
        return code;
    }
}
