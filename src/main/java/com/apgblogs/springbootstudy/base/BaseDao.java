package com.apgblogs.springbootstudy.base;

import com.github.pagehelper.Page;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author xiaomianyang
 * @description 增删改查基础类
 * @date 2019-04-28 上午 11:56
 */
public interface BaseDao<T extends BaseModel> {

    int deleteByPrimaryKey(String id);

    int insertSelective(T record);

    T selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(T record);

    int updateByPrimaryKey(T record);

    List<T> selectAll(@Param("record") T record, @Param("page") Page page);
}
