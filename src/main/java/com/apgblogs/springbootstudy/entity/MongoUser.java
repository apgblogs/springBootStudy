package com.apgblogs.springbootstudy.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.io.Serializable;
import java.util.List;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-06-30 18:26
 */
@Document(collection = "user")
public class MongoUser implements Serializable {

    @Id
    private String id;

    @Field("user_name")
    private String userName=null;

    private String note=null;

    @Field("roles")
    private List<MongoRole> roleList=null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public List<MongoRole> getRoleList() {
        return roleList;
    }

    public void setRoleList(List<MongoRole> roleList) {
        this.roleList = roleList;
    }
}
