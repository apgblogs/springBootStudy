package com.apgblogs.springbootstudy.entity;

import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author xiaomianyang
 * @description
 * @date 2019-06-30 18:28
 */

public class MongoRole {

    private String id;

    @Field("role_name")
    private String roleName=null;

    private String note=null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
